package com.quickmdp.data

import com.quickmdp.main.model.DiscoverData
import com.quickmdp.main.model.MovieInfo
import io.reactivex.Observable

/**
 * Created by @Y4583L on 9/15/17.
 */
class DataManager(private var mQuickMDPService: QuickMDPService) {

    fun getMovies(releaseDate: String?, page: Int): Observable<DiscoverData> {
        return if (releaseDate.isNullOrEmpty()) {
            mQuickMDPService.getMovies(page)
        } else {
            mQuickMDPService.getMoviesByGteReleaseDate(releaseDate!!, page)
        }
    }

    fun getMovieDetail(movieId: Int): Observable<MovieInfo> {
        return mQuickMDPService.getMovieDetail(movieId)
    }

}