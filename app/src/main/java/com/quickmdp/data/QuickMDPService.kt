package com.quickmdp.data

import com.google.gson.GsonBuilder
import com.quickmdp.main.model.DiscoverData
import com.quickmdp.main.model.MovieInfo
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


/**
 * Created by @Y4583L on 9/15/17.
 */
interface QuickMDPService {

    @GET("discover/movie?api_key=$API_KEY&language=en-US")
    fun getMovies(@Query("page") page: Int): Observable<DiscoverData>

    @GET("discover/movie?api_key=$API_KEY&language=en-US")
    fun getMoviesByGteReleaseDate(@Query(RELEASE_DATE) releaseDate: String, @Query("page") page: Int): Observable<DiscoverData>

    @GET("movie/{movieId}?api_key=$API_KEY")
    fun getMovieDetail(@Path("movieId") movieId: Int): Observable<MovieInfo>

    companion object Factory {
        private const val ENDPOINT = "https://api.themoviedb.org/3/"
        private const val RELEASE_DATE = "release_date.gte"
        const val API_KEY = "2d41ccbf0c97967a67260c800fc736bc"

        fun makeMDPService(): QuickMDPService {
            val okHttpClient = OkHttpClient.Builder().build()
            val gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd")
                    .create()
            val retrofit = Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return retrofit.create(QuickMDPService::class.java)
        }
    }

}