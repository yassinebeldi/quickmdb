package com.quickmdp.details

import com.quickmdp.base.Presenter
import com.quickmdp.data.DataManager
import com.quickmdp.utils.addTo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * Created by @Y4583L on 9/15/17.
 */
class DetailsPresenter(private var mDataManager: DataManager) : Presenter<DetailsMvpView> {

    private var mvpView: DetailsMvpView? = null
    private var mCompositeDisposable = CompositeDisposable()

    override fun attachView(mvpView: DetailsMvpView) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        mvpView = null
        mCompositeDisposable.clear()
    }

    fun fetchMovieDetail(movieId: Int) {
        Timber.i("Start fetching movie detail ")
        mvpView?.showProgress(true)
        mDataManager.getMovieDetail(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            Timber.d("Fetching movie detail called onSuccess $it")
                            mvpView?.onMovieDetailsSuccess(it)
                            mvpView?.showProgress(false)
                        },
                        {
                            it.printStackTrace()
                            mvpView?.showProgress(false)
                        })
                .addTo(mCompositeDisposable)
    }
}