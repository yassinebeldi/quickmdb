package com.quickmdp.details

import com.quickmdp.base.MvpView
import com.quickmdp.main.model.MovieInfo

/**
 * Created by @Y4583L on 9/15/17.
 */
interface DetailsMvpView : MvpView {
    fun showProgress(show: Boolean)
    fun onMovieDetailsSuccess(movie: MovieInfo)
}