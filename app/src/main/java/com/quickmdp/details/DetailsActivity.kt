package com.quickmdp.details

import android.os.Bundle
import android.widget.Toast
import com.quickmdp.R
import com.quickmdp.base.BaseActivity
import com.quickmdp.data.DataManager
import com.quickmdp.data.QuickMDPService
import com.quickmdp.main.model.MovieBasic
import com.quickmdp.main.model.MovieInfo
import com.quickmdp.utils.NetworkUtil
import com.quickmdp.utils.getDateTimeFormat
import com.quickmdp.utils.loadImage
import com.quickmdp.utils.setVisible
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_details.*

class DetailsActivity : BaseActivity(), DetailsMvpView {

    private var presenter: DetailsPresenter = DetailsPresenter(DataManager(QuickMDPService.makeMDPService()))

    companion object {
        val ARG_MOVIE = "ARG_MOVIE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener({ onBackPressed() })
        presenter.attachView(this)
        if (NetworkUtil.isNetworkConnected(this)) {
            loadData()
        } else {
            Toast.makeText(applicationContext, getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun loadData() {
        val movie = intent?.getSerializableExtra(ARG_MOVIE) as MovieBasic
        presenter.fetchMovieDetail(movie.movieId!!)
        toolbar.title = movie.title
    }

    override fun showProgress(show: Boolean) {
        mProgressBar.setVisible(show)
    }

    override fun onMovieDetailsSuccess(movie: MovieInfo) {
        movie_img.loadImage(movie.backdrop_path)
        movie_title.text = movie.title
        rating.text = getString(R.string.vote_based_on, movie.voteAverage, movie.voteCount)
        rating.setVisible(true)
        overview.text = getString(R.string.overview).plus("\n ${movie.overview}")
        original_title.text = getString(R.string.original_title).plus("\n ${movie.originalTitle}")
        release_date.text = getString(R.string.release_date).plus("${movie.releaseDate?.time?.getDateTimeFormat(this)}")
    }

    override fun onStop() {
        presenter.detachView()
        super.onStop()
    }
}
