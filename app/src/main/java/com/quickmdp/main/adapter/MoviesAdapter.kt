package com.quickmdp.main.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.quickmdp.R
import com.quickmdp.main.model.MovieBasic
import com.quickmdp.utils.getDateTimeFormat
import com.quickmdp.utils.inflate
import com.quickmdp.utils.loadImage
import com.quickmdp.utils.setVisible

/**
 * Created by @Y4583L on 9/16/17.
 */
class MoviesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_MOVIE = 0
    private val TYPE_FOOTER = 1

    private var movies = ArrayList<MovieBasic>()
    var isLoading = false
    var movieClick: ((MovieBasic) -> Unit)? = null

    fun setMovies(movies: List<MovieBasic>) {
        this.movies.clear()
        this.movies.addAll(movies)
        this.notifyDataSetChanged()
    }

    fun addMovies(movies: List<MovieBasic>) {
        this.movies.addAll(movies)
        this.notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int = when (position) {
        itemCount - 1 -> TYPE_FOOTER
        else -> TYPE_MOVIE
    }

    override fun getItemCount(): Int = movies.size + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        TYPE_MOVIE -> {
            val view = parent.inflate(R.layout.item_movie_card)
            val holder = MovieViewHolder(view, parent.context)
            holder
        }
        TYPE_FOOTER -> FooterViewHolder(ProgressBar(parent.context))
        else -> throw IllegalArgumentException("Unknown view holder type")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        when (holder) {
            is MovieViewHolder -> {
                holder.bind(movies[position])
                holder.cardView.setOnClickListener { movieClick?.invoke(movies[position]) }
            }
            is FooterViewHolder -> holder.bind(isLoading)
            else -> throw IllegalArgumentException("Unknown holder")
        }
    }

    class MovieViewHolder(itemView: View, private var context: Context) : RecyclerView.ViewHolder(itemView) {
        private val movieImage: ImageView = itemView.findViewById(R.id.movieImage)
        private val textTitle = itemView.findViewById<TextView>(R.id.title)
        private val description = itemView.findViewById<TextView>(R.id.overview)
        val cardView : CardView = itemView.findViewById(R.id.cardView)
        private val release = itemView.findViewById<TextView>(R.id.release)
        private val adult = itemView.findViewById<TextView>(R.id.adult)

        fun bind(movieInfo: MovieBasic) {
            textTitle.text = movieInfo.title
            description.text = movieInfo.overview
            release.text = movieInfo.releaseDate?.time?.getDateTimeFormat(context)
            adult.setVisible(movieInfo.adult ?: true)
            movieImage.loadImage(movieInfo.posterPath)
        }
    }

    class FooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val progress = itemView

        init {
            val layoutParams = StaggeredGridLayoutManager.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.isFullSpan = true
            itemView.layoutParams = layoutParams
        }

        fun bind(visible: Boolean) {
            progress.setVisible(visible)
            itemView.layoutParams.height = if (visible) RelativeLayout.LayoutParams.WRAP_CONTENT else 0
        }
    }
}