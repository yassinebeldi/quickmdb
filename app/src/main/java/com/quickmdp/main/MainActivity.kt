package com.quickmdp.main

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.quickmdp.R
import com.quickmdp.base.BaseActivity
import com.quickmdp.data.DataManager
import com.quickmdp.data.QuickMDPService
import com.quickmdp.details.DetailsActivity
import com.quickmdp.main.adapter.MoviesAdapter
import com.quickmdp.main.model.MovieBasic
import com.quickmdp.utils.EndlessRecyclerOnScrollListener
import com.quickmdp.utils.NetworkUtil
import com.quickmdp.utils.setVisible
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : BaseActivity(), MainMVPView {
    private var presenter: MainPresenter = MainPresenter(DataManager(QuickMDPService.makeMDPService()))
    private var page = 1
    private var max_pages = 1
    private var dateFormat = "yyyy-MM-dd"
    private var sdf = SimpleDateFormat(dateFormat, Locale.US)
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        presenter.attachView(this)
        adapter = MoviesAdapter()

        if (NetworkUtil.isNetworkConnected(this)) {
            presenter.fetchMovies()
        } else {
            Toast.makeText(applicationContext, "Please check your connexion", Toast.LENGTH_SHORT).show()
            showEmptyText(true)
            mNoDataTextView.text = getString(R.string.check_internet)
        }
    }

    override fun showProgress(show: Boolean) {
        mProgressBar.setVisible(show)
    }

    override fun showRecyclerView(show: Boolean) {
        mRecyclerView.setVisible(show)
    }

    override fun showEmptyText(show: Boolean) {
        mNoDataTextView.setVisible(show)
    }

    override fun onStop() {
        presenter.detachView()
        super.onStop()
    }

    override fun onMoviesSuccess(moviesBasic: List<MovieBasic>) {
        adapter.isLoading = false
        adapter.addMovies(moviesBasic)
    }

    override fun onMoviesSuccess(moviesBasic: List<MovieBasic>, totalPage: Int) {
        max_pages = totalPage
        adapter.setMovies(moviesBasic)
        mRecyclerView.adapter = adapter
        adapter.movieClick = { openDetailsScreen(it) }
        //Start listening for scroll events
        mRecyclerView.addOnScrollListener(EndlessRecyclerOnScrollListener(3, {
            page++
            if (page in 1..(max_pages - 1)) {
                adapter.isLoading = true
                presenter.fetchMovies(null, page)
            }
        }))
    }


    override fun onError(throwable: Throwable?) {
        mNoDataTextView.text = throwable?.localizedMessage
    }

    private fun openDetailsScreen(movieBasic: MovieBasic) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.ARG_MOVIE, movieBasic)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_release -> {
                DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show()
                return true
            }
        //clear filter
            R.id.action_clear_filter -> {
                page = 1
                presenter.fetchMovies()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private val myCalendar = Calendar.getInstance()
    private var date: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        //init page
        page = 1
        //fetch Movies which release date is greater or equal selected date
        presenter.fetchMovies(sdf.format(myCalendar.time))
    }

}
