package com.quickmdp.main

import com.quickmdp.base.MvpView
import com.quickmdp.main.model.MovieBasic

/**
 * Created by @Y4583L on 9/15/17.
 */
interface MainMVPView : MvpView {
    fun showProgress(show: Boolean)
    fun showRecyclerView(show: Boolean)
    fun showEmptyText(show: Boolean)
    fun onMoviesSuccess(moviesBasic: List<MovieBasic>)
    fun onMoviesSuccess(moviesBasic: List<MovieBasic>, totalPage: Int)
    fun onError(throwable: Throwable?)
}