package com.quickmdp.main

import com.quickmdp.base.Presenter
import com.quickmdp.data.DataManager
import com.quickmdp.utils.addTo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * Created by @Y4583L on 9/15/17.
 */
class MainPresenter(private var mDataManager: DataManager) : Presenter<MainMVPView> {

    private var mvpView: MainMVPView? = null
    private var mCompositeDisposable = CompositeDisposable()

    override fun attachView(mvpView: MainMVPView) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        mvpView = null
        mCompositeDisposable.clear()
    }

    fun fetchMovies(releaseDate: String? = null, page: Int = 1) {
        Timber.i("Start fetching movies")
        mvpView?.showEmptyText(false)
        if (page == 1) {
            mvpView?.showRecyclerView(false)
            mvpView?.showProgress(true)
        }
        mDataManager.getMovies(releaseDate, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    Timber.d("Fetching movies called onSuccess ${it.results?.size ?: 0}")
                    if (it.results?.isNotEmpty() != null) {
                        if (page == 1) {
                            mvpView?.showRecyclerView(true)
                            mvpView?.onMoviesSuccess(it.results!!, it.totalPages ?: 1)
                        } else {
                            mvpView?.onMoviesSuccess(it.results!!)
                        }
                    } else {
                        mvpView?.showEmptyText(true)
                    }
                    mvpView?.showProgress(false)
                    //onNext
                }, {
                    it.printStackTrace()
                    mvpView?.showProgress(false)
                    //onError
                })
                .addTo(mCompositeDisposable)
    }

    fun discoverMovies(releaseDate: String?) {
        Timber.i("Start fetching movies released after $releaseDate")

    }
}