package com.quickmdp.main.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * Created by @Y4583L on 9/18/17.
 */
open class MovieBasic : Serializable {

    @SerializedName("id")
    var movieId: Int? = null
    @SerializedName("vote_count")
    var voteCount: Int? = null
    @SerializedName("vote_average")
    var voteAverage: Float? = null
    var title: String? = null
    @SerializedName("poster_path")
    var posterPath: String? = null
    @SerializedName("original_language")
    var originalLanguage: String? = null
    @SerializedName("original_title")
    var originalTitle: String? = null
    var adult: Boolean? = null
    var overview: String? = null
    @SerializedName("release_date")
    var releaseDate: Date? = null
}