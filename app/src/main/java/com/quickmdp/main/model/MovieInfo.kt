package com.quickmdp.main.model

import java.io.Serializable

/**
 * Created by @Y4583L on 9/16/17.
 */
class MovieInfo : MovieBasic(), Serializable {
    //needed in details screen
    var backdrop_path: String? = null
    var genres: List<Genre>? = null
    var runtime: Int? = null
}

class Genre : Serializable {
    var name: String? = null
}
