package com.quickmdp.main.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by @Y4583L on 9/16/17.
 */
class DiscoverData : Serializable {
    var page: Int? = null
    @SerializedName("total_results")
    var totalResults: Int? = null
    @SerializedName("total_pages")
    var totalPages: Int? = null
    var results: MutableList<MovieInfo>? = null
}