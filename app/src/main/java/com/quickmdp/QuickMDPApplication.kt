package com.quickmdp

import android.app.Application
import timber.log.Timber


/**
 * Created by @Y4583L on 9/16/17.
 */
class QuickMDPApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }

}
