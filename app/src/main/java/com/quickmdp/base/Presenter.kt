package com.quickmdp.base

/**
 * Created by @Y4583L on 9/15/17.
 */
interface Presenter<in V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()
}
