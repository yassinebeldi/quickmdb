package com.quickmdp.base

/**
 * Base interface that any class that wants to act as a View in the MVP (Model View Presenter)
 * pattern must implement.
 * Created by @Y4583L on 9/15/17.
 */

interface MvpView