package com.quickmdp.utils

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.quickmdp.R
import com.squareup.picasso.Picasso
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @Y4583L on 9/16/17.
 */

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

/**
 * Toggles view visible state setting View.GONE if not visible
 */
fun View.setVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

/**
 * Adds disposable to composite disposable
 */
fun Disposable.addTo(disp: CompositeDisposable) {
    disp.add(this)
}


/*
    Load the images using Picasso library
 */
fun ImageView.loadImage(url: String?) {
    if (url.isNullOrEmpty()) {
        setImageResource(R.drawable.image_not_found)
    } else
        Picasso.with(context)
                .load("https://image.tmdb.org/t/p/w1280/$url")
                .placeholder(R.drawable.image_not_found)
                .error(R.drawable.image_not_found)
                .into(this)
}

fun ImageView.cancel() {
    Picasso.with(context).cancelRequest(this)
}

fun Long.getDateTimeFormat(context: Context): String =
        DateUtils.formatDateTime(context, this, DateUtils.FORMAT_ABBREV_ALL or DateUtils.FORMAT_SHOW_YEAR)

fun Long.getDateFromMillis(): String {
    val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
    return formatter.format(Date(this))
}
