package com.quickmdp.utils

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager

/**
 * Created by @Y4583L on 9/16/17.
 */
class EndlessRecyclerOnScrollListener(
        var visibleThreshold: Int = 5, //Number of items at the bottom when we starting loading, 5 by default
        val loadMore: () -> Unit
) : RecyclerView.OnScrollListener() {
    var previousTotal = 0 //The total number of items in the dataset after the last load
    var loading = true//* True if we are still waiting for the last set of data to load.

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val firstVisibleItem = getFirstVisiblePosition(recyclerView)
        if (firstVisibleItem != null) {
            val visibleItemCount = recyclerView.childCount
            val totalItemCount = recyclerView.layoutManager.itemCount

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false
                    previousTotal = totalItemCount
                }
            }

            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // End has been reached
                loading = true
                loadMore()
            }
        }
    }

    /**
     * Returns first visible item position for recycler view based on LayoutManager
     * It can return null if there is no layoutManager or layoutManager
     * is not LinearLayoutManager or StaggeredGridLayoutManager
     */
    fun getFirstVisiblePosition(recyclerView: RecyclerView): Int? {
        recyclerView.layoutManager?.let {
            when (it) {
                is LinearLayoutManager -> {
                    return it.findFirstVisibleItemPosition()
                }

                is StaggeredGridLayoutManager -> {
                    it.findFirstVisibleItemPositions(null)?.let {
                        if (it.isNotEmpty()) {
                            return it[0]
                        }
                    }
                }

                else -> return null
            }
        }
        return null
    }
}
